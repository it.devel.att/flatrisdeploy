FROM node:15.12.0-alpine3.10

COPY . .

RUN yarn install
RUN yarn build
CMD ["yarn", "start"]
